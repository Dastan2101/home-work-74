const express = require('express');
const fs = require('fs');

const path = "./messages";

const router = express.Router();

router.get('/', (req, res) => {

    const arrayFiles = [];

    fs.readdir(path, (err, files) => {

        for (let i = files.length - 5; i < files.length; i++) {
            let message = fs.readFileSync(`${path}/${files[i]}`);
            arrayFiles.push(JSON.parse(message))
        }
        res.send(arrayFiles)


    })
});

router.post('/', (req, res) => {
    let date = new Date();

    const messageFile = `./messages/${date}.txt`;

    fs.writeFileSync(messageFile, JSON.stringify({message: req.body.message, datetime: date}));

    res.send({message: 'Success'})
});

module.exports = router;
